# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :liveview_exemple,
  ecto_repos: [LiveviewExemple.Repo]

# Configures the endpoint
config :liveview_exemple, LiveviewExempleWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pWm4E/2IXP/3uq+FqSaX5WH66WKplbmekJQHF8x9wS4PDqkufGSMY3bHZvqFL+IX",
  render_errors: [view: LiveviewExempleWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: LiveviewExemple.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "7xwAgtPA3inNXCf6h8ffI4pAZt1whjE9"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
