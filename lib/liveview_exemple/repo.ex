defmodule LiveviewExemple.Repo do
  use Ecto.Repo,
    otp_app: :liveview_exemple,
    adapter: Ecto.Adapters.Postgres
end
