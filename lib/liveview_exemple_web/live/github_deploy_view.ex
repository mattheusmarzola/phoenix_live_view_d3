defmodule MyAppWeb.GithubDeployView do
    use Phoenix.LiveView
  
    def render(assigns) do
      ~L"""
      <script src="https://d3js.org/d3.v4.js"></script>
      <div class="">
        <div>
          <div>
            <button phx-click="change_bins">Change Bins</button>
          </div>
          <div id="my_dataviz"></div>
          <p>
            <label># bins</label>
            <input type="number" min="1" max="100" step="30" value="<%= @bins_number %>" phx-hook="index" id="nBin">
          </p>
        </div>
      </div>
      """
    end
  
    def mount(_session, socket) do
      {:ok, assign(socket, bins_number: Enum.random(10..100))}
    end

    def handle_event("change_bins", _value, socket) do
      IO.puts("aaaaaaaaa")
      {:noreply, assign(socket, bins_number: Enum.random(10..100))}
    end
    
end