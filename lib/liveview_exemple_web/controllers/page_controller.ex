defmodule LiveviewExempleWeb.PageController do
  use LiveviewExempleWeb, :controller
  alias Phoenix.LiveView

  def index(conn, _params) do
    LiveView.Controller.live_render(conn, MyAppWeb.GithubDeployView, session: %{})
  end
end
