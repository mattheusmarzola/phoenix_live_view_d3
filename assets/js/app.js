// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"
import "mdn-polyfills/NodeList.prototype.forEach"
import "mdn-polyfills/Element.prototype.closest"
import "mdn-polyfills/Element.prototype.matches"
import "child-replace-with-polyfill"
import "url-search-params-polyfill"
import "formdata-polyfill"
import "classlist-polyfill"

import LiveSocket from "phoenix_live_view"

const createD3 = () =>{
    var margin = {top: 10, right: 30, bottom: 30, left: 40},
    width = 460 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#my_dataviz")
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");
    return svg;
}

const updateGraph = (data, nBin, graph) =>{
    var margin = {top: 10, right: 30, bottom: 30, left: 40},
    width = 460 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    var x = d3.scaleLinear().domain([0, 1000])     // can use this instead of 1000 to have the max of data: d3.max(data, function(d) { return +d.price })
    .range([0, width]);
    graph.append("g").attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

    // Y axis: initialization
    var y = d3.scaleLinear().range([height, 0]);
    var yAxis = graph.append("g")

    // set the parameters for the histogram
    var histogram = d3.histogram()
                        .value(function(d) { return d.price; })   // I need to give the vector of value
                        .domain(x.domain())  // then the domain of the graphic
                        .thresholds(x.ticks(nBin)); // then the numbers of bins

    // And apply this function to data to get the bins
    var bins = histogram(data);

    // Y axis: update now that we know the domain
    y.domain([0, d3.max(bins, function(d) { return d.length; })]);   // d3.hist has to be called before the Y axis obviously
    yAxis
    .transition()
    .duration(1000)
    .call(d3.axisLeft(y));

    // Join the rect with the bins data
    var u = graph.selectAll("rect")
    .data(bins)

    // Manage the existing bars and eventually the new ones:
    u.enter()
    .append("rect") // Add a new rect for each new elements
    .merge(u) // get the already existing elements as well
    .transition() // and apply changes to all of them
    .duration(1000)
    .attr("x", 1)
    .attr("transform", function(d) { return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
    .attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
    .attr("height", function(d) { return height - y(d.length); })
    .style("fill", "#69b3a2")


    // If less bar in the new histogram, I delete the ones not in use anymore
    u.exit().remove()
}



let hooks = {
  index: {
    mounted() {
        console.log("mounted")
        let binsNumber = this.el.value;
        var graph = createD3();
        var app = this;
        // get the data
        d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv")
        .get(function(error, rows) {
            let data_api = rows;
            updateGraph(data_api, binsNumber, graph)
            Object.assign(app, { data_api });
        });
    },
    updated(){
        console.log("updated")

        let {data_api} = this
        var graph = createD3();
        updateGraph(data_api, this.el.value, graph)
    }
  }
}

let liveSocket = new LiveSocket("/live", {hooks})
liveSocket.connect()

